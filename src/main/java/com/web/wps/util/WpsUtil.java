package com.web.wps.util;

import com.web.wps.config.ReqKeys;
import com.web.wps.propertie.RedirectProperties;
import com.web.wps.propertie.WpsProperties;
import com.web.wps.util.file.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class WpsUtil {

    private final WpsProperties wpsProperties;
    private final RedirectProperties redirect;

    @Autowired
    public WpsUtil(WpsProperties wpsProperties, RedirectProperties redirect) {
        this.wpsProperties = wpsProperties;
        this.redirect = redirect;
    }

    public String getWpsUrl(Map<String, String> values, String fileType, String fileId) {
        String keyValueStr = SignatureUtil.getKeyValueStr(values);
        String signature = SignatureUtil.getSignature(values, wpsProperties.getAppsecret());
        String fileTypeCode = FileUtil.getFileTypeCode(fileType);

        return wpsProperties.getDomain() + fileTypeCode + "/" + fileId + "?"
                + keyValueStr + ReqKeys.SIGNATURE_KEY + "=" + signature;
    }

    public String getTemplateWpsUrl(String fileType, String userId) {
        Map<String, String> values = new HashMap<String, String>() {
            {
                put(redirect.getKey(), redirect.getValue());
                put(ReqKeys.APP_ID_KEY, wpsProperties.getAppid());
                put(ReqKeys.USER_ID_KEY, userId);
            }
        };
        String keyValueStr = SignatureUtil.getKeyValueStr(values);
        String signature = SignatureUtil.getSignature(values, wpsProperties.getAppsecret());
        String fileTypeCode = FileUtil.getTypeCode(fileType);

        return wpsProperties.getDomain() + fileTypeCode + "/new/0" + "?"
                + keyValueStr + ReqKeys.SIGNATURE_KEY + "=" + signature;
    }

}
