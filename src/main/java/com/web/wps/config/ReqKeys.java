package com.web.wps.config;

public class ReqKeys {
    public final static String TOKEN_KEY = "x-wps-weboffice-token";
    public final static String FILE_ID_KEY = "x-weboffice-file-id";
    public final static String USER_AGENT_KEY = "User-Agent";

    public final static String APP_ID_KEY = "_w_appid";
    public final static String SIGNATURE_KEY = "_w_signature";

    public final static String USER_ID_KEY = "_w_userid";

    public final static String TOKEN_TYPE_KEY = "_w_tokentype";
    public final static String FILE_PATH_KEY = "_w_filepath";
    public final static String FILE_TYPE_KEY = "_w_filetype";

    public final static String SECRET_KEY = "_w_secretkey";
}
